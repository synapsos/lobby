var User = require('../models/user');
var Conversations = function () {
  this.respondsWith = ['html', 'json', 'xml', 'js', 'txt'];

  this.index = function (req, resp, params) {
    var self = this;

    geddy.model.Conversation.all(function(err, conversations) {
      self.respond({params: params, conversations: conversations});
    });
  };

  this.add = function (req, resp, params) {
    this.respond({params: params});
  };

  this.create = function (req, resp, params) {
    var self = this
      , conversation = geddy.model.Conversation.create({
            subject: params.subject
        });

    if (!conversation.isValid()) {
      params.errors = conversation.errors;
      self.transfer('add');
    }
    
    conversation.save(function(err, data) {
      if (err) {
        params.errors = err;
        self.transfer('add');
      } else {
        if (req.user) conversation.setOwner(req.user);
        self.redirect({controller: self.name});
      }
    });
  };

  this.show = function (req, resp, params) {
    var self = this;

    geddy.model.Conversation.first(params.id, function(err, conversation) {
      if (!conversation) {
        var err = new Error();
        err.statusCode = 400;
        self.error(err);
      } else {
        self.respond({params: params, conversation: conversation.toObj()});
      }
    });
  };

  this.edit = function (req, resp, params) {
    var self = this;

    geddy.model.Conversation.first(params.id, function(err, conversation) {
      if (!conversation) {
        var err = new Error();
        err.statusCode = 400;
        self.error(err);
      } else {
        self.respond({params: params, conversation: conversation});
      }
    });
  };

  this.update = function (req, resp, params) {
    var self = this;

    geddy.model.Conversation.first(params.id, function(err, conversation) {
      conversation.updateProperties(params);
      if (!conversation.isValid()) {
        params.errors = conversation.errors;
        self.transfer('edit');
      }

      conversation.save(function(err, data) {
        if (err) {
          params.errors = err;
          self.transfer('edit');
        } else {
          self.redirect({controller: self.name});
        }
      });
    });
  };

  this.destroy = function (req, resp, params) {
    var self = this;

    geddy.model.Conversation.remove(params.id, function(err) {
      if (err) {
        params.errors = err;
        self.transfer('edit');
      } else {
        self.redirect({controller: self.name});
      }
    });
  };

};

exports.Conversations = Conversations;
